<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="Studenti">
    
    <h1> Students</h1>
    
    <div class="row">
            <div class="col-md-1">
                Nume
            </div>
            <div class="col-md-1">
                Prenume
            </div>
           
            <div class="col-md-2">
                Specializarea
            </div>
             <div class="col-md-2">
                Media
            </div>
            <div class="col-md-2">
                CNP
            </div>
            
            <div class="col-md-1">
                Status
            </div>
            
        </div>
    <hr>
    <c:forEach var="student" items="${students}" varStatus="status">
        <div class="row">
            <div class="col-md-1">
                ${student.nume}
            </div>
            <div class="col-md-1">
                ${student.prenume}
            </div>
           
            <div class="col-md-2">
                ${student.cnp}
            </div>
             <div class="col-md-2">
                ${student.medie}
            </div>
            <div class="col-md-2">
                ${student.specializare}
            </div>
            
            <div class="col-md-1">
                ${student.status}
            </div>
            <div class="col-md-1">
                <a class="btn btn-secondary" href="${pageContext.request.contextPath}/EditStatus?id=${student.id}" role="button">Edit</a>
            </div>
        </div>
            <hr>
    </c:forEach>
    
</t:pageTemplate>
