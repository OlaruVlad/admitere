<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="Mesaje">
    
    <h1> Messages</h1>
    <form method="POST" action="${pageContext.request.contextPath}/Messages">
    <button class="btn btn-danger" type="submit">Delete Message </button>
    <c:forEach var="contact" items="${contacts}" varStatus="status">
        <div class="row">
            
            <div class="col-md-4">
                ${contact.name}
            </div>
            <div class="col-md-3">
                ${contact.email}
            </div>
            <div class="col-md-3">
                ${contact.message}
            </div>
                
            <div class="col-md-1">
              <input type="checkbox" name="msg_ids" value="${contact.id}" />
           </div>
            </div>
    </c:forEach>
    </form>
</t:pageTemplate>
