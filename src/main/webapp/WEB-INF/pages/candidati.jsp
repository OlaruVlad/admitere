<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="Studenti">
    
    <h1> Students</h1>
    
    <div class="row">
            <div class="col-md-2">
                Nume
            </div>
            <div class="col-md-2">
                Prenume
            </div>
           
             <div class="col-md-2">
                Media
            </div>
            <div class="col-md-2">
               Specializarea
            </div>
            
            <div class="col-md-2">
                Status
            </div>
        </div>
    <hr>
    
    <c:forEach var="student" items="${students}" varStatus="status">
        <div class="row">
            <div class="col-md-2">
                ${student.nume}
            </div>
            <div class="col-md-2">
                ${student.prenume}
            </div>
           
             <div class="col-md-2">
                ${student.medie}
            </div>
            <div class="col-md-2">
                ${student.cnp}
            </div>
            
            <div class="col-md-2">
                ${student.status}
            </div>
        </div>
            <hr>
    </c:forEach>
    
</t:pageTemplate>
