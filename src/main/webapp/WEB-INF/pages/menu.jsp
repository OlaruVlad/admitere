<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container">
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="${pageContext.request.contextPath}">Admitere</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">      
       <li class="nav-item ${activePage eq 'About' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/About">About </a>
      </li>
       <c:if test="${pageContext.request.isUserInRole('ComisieRole')}" >
     
          
      <li class="nav-item ${activePage eq 'Users' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Users">Users</a>
      </li>
      
      <li class="nav-item ${activePage eq 'Students' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Students">Studenti</a>
      </li>
      
      <li class="nav-item ${activePage eq 'Messages' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Messages">Mesaje</a>
      </li>
        
      </c:if>
      
       <c:if test="${pageContext.request.isUserInRole('StudentRole')}">
      <li class="nav-item ${activePage eq 'Test' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Test">Contact</a>
      </li>
      <li class="nav-item ${activePage eq 'Aplica' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Aplica">Aplica</a>
      </li>
       <li class="nav-item ${activePage eq 'Candidati' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Candidati">Lista candidati</a>
      </li>
       </c:if>

     
    <ul class="navbar-nav ml-auto">
        <li class="navbar-nav ml-auto">
        <c:choose> 
            <c:when test="${pageContext.request.getRemoteUser()==null}">
            <a class="nav-link" href="${pageContext.request.contextPath}/Login"> Login </a>
             <li class="nav-item ${activePage eq 'Register' ? 'active' : ''}">
        <a class="nav-link" href="${pageContext.request.contextPath}/Register">Register</a>
      </li>
            </c:when>
            <c:otherwise>
                <a class="nav-link" href="${pageContext.request.contextPath}/Logout">Logout</a>
            </c:otherwise>   
        </c:choose>
        </li>
    </ul>
  </div>
</nav>
 </div>
          