<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="About">
    
       <section class="header1 cid-qyvisAZ1Bw" id="header1-7" data-rv-view="758">

    

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-5">Responsive Bootstrap About Us Page Template</h1>
                <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-2">MEET OUR TEAM</h3>
                <p class="mbr-text align-center pb-3 mbr-fonts-style display-5">
                    We are all very different. We were born in different cities, at different times, we love different music, food, movies. But we have something that unites us all. It is our company. We are its heart. We are not just a team, we are a family.
                </p>
     
        </div>
    </div>

</section>

<section class="header1 cid-qyvcqB36eR" id="header1-0" data-rv-view="761">

    

    

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                
                
                
                
            </div>
        </div>
    </div>

</section>

<section class="features1 cid-qyvcAqZyNq" id="features1-1" data-rv-view="764">
    
    

    
    <div class="container">
        <div class="media-container-row">

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-users" style="color: rgb(0, 0, 0);" media-simple="true"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">
                        Creativity</h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                        It's the ability to think outside the box. We make decisions, create something new and generate a lot of ideas.
                    </p>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-globe" style="color: rgb(0, 0, 0);" media-simple="true"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">
                        Worldwide</h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                        All sites you make with Mobirise are mobile-friendly. You don't have to create a special mobile version of your site.
                    </p>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-smile-face" style="color: rgb(0, 0, 0);" media-simple="true"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">
                        Unique Styles
                    </h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                        Mobirise offers many site blocks in several themes, and though these blocks are pre-made, they are flexible.
                    </p>
                </div>
            </div>

            

        </div>

    </div>

</section>
    
<section class="mbr-section content4 cid-qxWA1sL9mH" id="content4-oq" data-rv-view="9508">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                    <h5 class="align-center pb-3 mbr-fonts-style display-7"><br>© Copyright 2020 Echipa 11 </a> - (not)All Rights Reserved </h5>
            </div>
        </div>
    </div>
</section>



    
</t:pageTemplate>
