<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<t:pageTemplate pageTitle="Contact">
    <h1>Contact Us</h1>
    
  <form class="needs-validation" novalidate method="POST" action="${pageContext.request.contextPath}/Test">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="username">Name</label>
            <input type="text" class="form-control" id="username" name="name" placeholder="" value="" required>
            <div class="invalid-feedback">
             Username is required.
            </div>
          </div>
        </div>
             <div class="row">
          <div class="col-md-6 mb-3">
              <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="" value="" required>
            <div class="invalid-feedback">
              Email is required.
            </div>
          </div>
        </div>
        
                <div class="row">
          <div class="col-md-5 mb-3">
            <label for="message">Do you have a question?</label>
            <textarea class="form-control" id="message" name="message" placeholder="Your question" value="" required rows="5"></textarea>
            <div class="invalid-feedback">
                Message is required.
            </div>
          </div>
            </div>
  
        <button class="btn btn-primary btn-lg btn-block" type="submit">Send</button>
            
    </form>
        
     <script src="form-validation.js"></script>
</t:pageTemplate>
     <script>(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
    })
  }, false)
}())
     </script>