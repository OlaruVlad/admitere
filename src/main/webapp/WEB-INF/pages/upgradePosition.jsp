<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="Studenti">
    
    <h1> Promovare</h1>
    
    <form class="needs-validation" novalidate method="POST" action="${pageContext.request.contextPath}/UpgradePosition">
        <label for="position">Pozitia</label>
        <select class="custom-select d-block w-100" name="position" id="position" required>
            <option value="COMISIE">COMISIE</option>
        </select>
        <input type="hidden" name="user_id" value="${user.id}" />
        <button class="btn btn-primary btn-lg btn-block" type="submit">save</button>
    </form>
</t:pageTemplate>