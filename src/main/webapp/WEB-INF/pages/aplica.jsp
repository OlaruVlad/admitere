<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<t:pageTemplate pageTitle="Apply">
    <h1>Apply to us ! :)</h1>
    
  <form class="needs-validation" novalidate method="POST"  action="${pageContext.request.contextPath}/Aplica">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="fname">FirstName</label>
            <input type="text" class="form-control" id="fname" name="fname" placeholder="" value="" required>
            <div class="invalid-feedback">
             FirstName is required.
            </div>
          </div>     
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="lname">LastName</label>
            <input type="text" class="form-control" id="lname" name="lname" placeholder="" value="" required>
            <div class="invalid-feedback">
             LastName is required.
            </div>
          </div>     
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="cnp">CNP</label>
            <input type="text" class="form-control" id="cnp" name="cnp" placeholder="" value="" required>
            <div class="invalid-feedback">
              CNP is required.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="grade">Grade</label>
            <input type="text" class="form-control" id="grade" name="grade" placeholder="" value="" required>
            <div class="invalid-feedback">
              Grade is required.
            </div>
          </div>
        </div>
        <div class="row"> 
            <div class="col-md-6 mb-3">
                <label for="speciality">Speciality</label>
            <select class="custom-select d-block w-100" id="speciality" name="speciality" required>
              <option value="">Choose...</option>
              <option value="Computer Science">Computer Science </option>
              <option value="Information Technology">Information Technology</option>
              <option value="TCM">TCM</option>
              <option value="IEDM">IEDM</option>
            </select>
          </div>
            <div class="invalid-feedback">
              Please select a speciality.
            </div>
        </div>
        
  
        <button class="btn btn-primary btn-lg btn-block" type="submit">Send</button>
            
    </form>
        
     <script src="form-validation.js"></script>
</t:pageTemplate>
     <script>(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
    })
  }, false)
}())
     </script>