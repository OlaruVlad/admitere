<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>


<t:pageTemplate pageTitle="Users">
     
    <h1>users</h1>
    
    <c:forEach var="users" items="${users}" varStatus="status">
        <div class="row">
            <div class="col-md-2">
                ${users.username}
            </div>
            <div class="col-md-2">
                ${users.email}
            </div>

            <div class="col-md-2">
                ${users.role}
            </div>
     
           <div class="col-md-1">
                <a class="btn btn-info" href="${pageContext.request.contextPath}/UpgradePosition?id=${users.id}" role="button">Promoveaza ca Membru</a>
            </div>
        </div>
            <hr>
    </c:forEach>
            
</t:pageTemplate>
