<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<t:pageTemplate pageTitle="Studenti">
    
    <h1> Students</h1>
    
    <form class="needs-validation" novalidate method="POST" action="${pageContext.request.contextPath}/EditStatus">
        <label for="status">Status</label>
        <select class="custom-select d-block w-100" name="status" id="status" required>
            <option value="">Alege..</option>
            <option value="ADMIS">ADMIS</option>
            <option value="RESPINS">RESPINS</option>
        </select>
        <input type="hidden" name="student_id" value="${student.id}" />
        <button class="btn btn-primary btn-lg btn-block" type="submit">save</button>
    </form>
</t:pageTemplate>