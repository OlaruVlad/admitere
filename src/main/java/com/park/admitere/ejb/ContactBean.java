/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.park.admitere.ejb;

import com.park.admitere.common.ContactDetails;
import com.park.admitere.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Olaruuuu
 */
@Stateless
public class ContactBean {

    private static final Logger LOG = Logger.getLogger(ContactBean.class.getName());

   @PersistenceContext
   
   private EntityManager em;
   
   public List<ContactDetails> getAllContacts(){
       LOG.info("getAllContacts");
       try {  
            Query query=em.createQuery("SELECT m FROM Contact m");
            List <Contact> contacts = (List<Contact>) query.getResultList();           
            return copyContactToDetails(contacts);     
            } catch (Exception ex) { 
                throw new EJBException(ex);    
            }   
      }
       private List<ContactDetails> copyContactToDetails(List<Contact>contacts){
        List<ContactDetails>detailsList= new ArrayList<>();
        for(Contact contact : contacts){
            ContactDetails contactDetails= new ContactDetails(contact.getId(),
                                contact.getName(),
                                contact.getEmail(),
                                contact.getMessage());
            detailsList.add(contactDetails);
        }
        return detailsList;
    }

    public void createContact(String name, String email, String message) {
       Contact contact= new Contact();
       contact.setName(name);
       contact.setEmail(email);
       contact.setMessage(message);
       
       em.persist(contact);
    }
    
    public void deteleMsgByIds(Collection<Integer> ids)
    {
    LOG.info("deleteMsgByIds");
    for(Integer id:ids){
        Contact contact = em.find(Contact.class, id);
        em.remove(contact);
    }
    }
   }
