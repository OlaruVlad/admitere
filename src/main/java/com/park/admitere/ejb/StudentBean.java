/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.park.admitere.ejb;

import com.park.admitere.common.StudentDetails;
import com.park.admitere.entity.File;
import com.park.admitere.servlet.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Olaruuuu
 */
@Stateless
public class StudentBean {

    private static final Logger LOG = Logger.getLogger(StudentBean.class.getName());
    @PersistenceContext
    private EntityManager em;
    
     public void createStudent(String name, String lname, String cnp,String grade,String specilaity){
    Student student= new Student();
    student.setNume(name);
    student.setPrenume(lname);
    student.setCnp(cnp);
    student.setMedie(grade);
    student.setSpecializare(specilaity);
    
    em.persist(student);
    
    
    
}
     
     public StudentDetails findById(Integer studentId){
         Student student = em.find(Student.class,studentId);
         return new StudentDetails(student.getId(),student.getNume(),student.getPrenume(),
                 student.getSpecializare(),student.getMedie(),student.getCnp(),student.getStatus());
     }
    
//     public StudentDetails findById(Integer studentId){
//         Student student = em.find(Student.class,studentId);
//         return new StudentDetails(student.getId(),
//                                    student.getNume(),
//                                    student.getPrenume(),
//                                    student.getCnp(),
//                                    student.getMedie(),
//                                    student.getSpecializare());
//                                   
//     }
     
    public List<StudentDetails> getAllStudents() {
        LOG.info("getAllStudents");
        try {
            Query query=em.createQuery("SELECT s FROM Student s");
        List<Student> students = (List<Student>) query.getResultList();
        return copyStudentsToDetails(students);
        } catch (Exception ex) {
        throw new EJBException(ex);
        }
        }
    
//    public void addFileToStudent(Integer studentId, String filename,String fileType, byte[] fileContent)
//    {
//        LOG.info("addFileToCar");
//        File file = new File();
//        file.setFilename(filename);
//        file.setFiletype(fileType);
//        file.setFileContent(fileContent);
//        
//        Student student = em.find(Student.class,studentId);
//        student.setFile(file);
//        
//        file.setStudent(student);
//        em.persist(file);
//        
//    }
//    
        private List<StudentDetails> copyStudentsToDetails(List<Student> students){
     List<StudentDetails> detailsList = new ArrayList<>();
        for(Student student :students){
            StudentDetails studentDetails = new StudentDetails(student.getId(), 
                    student.getNume(), 
                    student.getPrenume(), 
                    student.getCnp(),
                    student.getMedie(),
                    student.getSpecializare(),                   
                    student.getStatus()
            );
        detailsList.add(studentDetails);
        }
    return detailsList;
    }
        
    public void updateStudent(String status, Integer student_id )
    {
    LOG.info("updateStatus");
    Student student = em.find(Student.class,student_id);
    
    student.setStatus(status);
    
    
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
}
