/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.park.admitere.common;

import java.io.Serializable;

/**
 *
 * @author Olaruuuu
 */
public class ContactDetails implements java.io.Serializable{
    
    private Integer id;
    private String name;
    private String email;
    private String message;
    
    public ContactDetails(Integer id, String name, String email, String message){
          this.id=id;
          this.name=name;
          this.email=email;
          this.message=message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
    

