/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.park.admitere.common;

/**
 *
 * @author Olaruuuu
 */
public class StudentDetails implements java.io.Serializable{
      private Integer id;
    
    private String nume;
    private String prenume;
    private String specializare;
    private String medie;
    private String cnp;
    private String status;

    public StudentDetails(Integer id, String nume, String prenume, String specializare, String medie, String cnp,String status) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.specializare = specializare;
        this.medie = medie;
        this.cnp = cnp;
        this.status=status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

 

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }


 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getSpecializare() {
        return specializare;
    }

    public void setSpecializare(String specializare) {
        this.specializare = specializare;
    }

    public String getMedie() {
        return medie;
    }

    public void setMedie(String medie) {
        this.medie = medie;
    }
    
    
}
