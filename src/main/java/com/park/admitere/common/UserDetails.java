/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.park.admitere.common;

/**
 *
 * @author Olaruuuu
 */
public class UserDetails implements java.io.Serializable {
    
    private Integer id;
    private String Username;
    private String Email;
    private String Role;

    public UserDetails(Integer id, String Username, String Email, String Role) {
        this.id = id;
        this.Username = Username;
        this.Email = Email;
        this.Role = Role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String Role) {
        this.Role = Role;
    }

  

  
    
    
   
    
}
